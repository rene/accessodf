accessodf (0.1.1~b-2) UNRELEASED; urgency=medium

  * control: Migrate priority to optional.
  * control: Bump Standards-Version to 4.4.0.
  * watch: Generalize pattern.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 02 Feb 2016 01:07:27 +0100

accessodf (0.1.1~b-1) unstable; urgency=low

  [ Samuel Thibault ]
  * Team upload

  [ Sebastian Humenda ]
  * rework symlinking infrastructure
  * section editors fits better than java (Closes: #710567)
  * import new upstream version
  * add debian-specific build infrastructure

  [ Samuel Thibault ]
  * Bump Standards-Version to 3.9.6 (no changes).
  * patches/no-addon: Disable building the accessodf add-on.
  * control: build-depend on maven-repo-helper, comment out add-on packages.
  * rules: Install maven information.
  * copyright: Update to LGPL-3 licence.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 01 Feb 2016 23:16:22 +0100

accessodf (0.1-4) unstable; urgency=low

  * add Replaces/Breaks for new packages, to not coexist with older versions
    of the package

 -- Sebastian Humenda <shumenda@gmx.de>  Mon, 20 May 2013 11:26:41 +0200

accessodf (0.1-3) unstable; urgency=low

  [ Sebastian Humenda ]
  * do not depend on openjdk-6 (Closes: #674578)
  * split package into a library and an extension, rename extension to
    libreoffice-accessodf (Closes: #684921)
  * bump standards-version

  [ Samuel Thibault ]
  * Fix duplicate installation of accessodf.jar.

 -- Sebastian Humenda <shumenda@gmx.de>  Thu, 16 May 2013 17:13:16 +0200

accessodf (0.1-2) unstable; urgency=low

  * {pre,post}{inst|rm}: removed. The cleaner (now working) solution is to
    just unzip the package. This method won't leave unowned files behind.
    (Closes: #679717)

 -- Sebastian Humenda <shumenda@gmx.de>  Mon, 21 Jan 2013 19:01:52 +0200

accessodf (0.1-1.3) unstable; urgency=low

  * Non-maintainer upload.
  * oops, forgot a unopkg in preinst, thanks Andreas Beckmann
    (closes: #679717)

 -- Rene Engelhard <rene@rene-engelhard.de>  Fri, 02 Nov 2012 12:32:50 +0100

accessodf (0.1-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * set $HOME explicitely in unopkg usage in maintainer scripts
    (closes: #679717)

 -- Rene Engelhard <rene@debian.org>  Tue, 23 Oct 2012 19:53:57 +0200

accessodf (0.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Build-depend and Depend on libcommons-collections3-java instead of
    libcommons-collections-java.
  * Depend on libreoffice-java-common (Closes: #668376)

 -- Samuel Thibault <sthibault@debian.org>  Sat, 30 Jun 2012 00:28:15 -0300

accessodf (0.1-1) unstable; urgency=low

  * Initial release, packaged as a dependency for odt2braille. (Closes: #668144)

 -- Sebastian Humenda <shumenda@gmx.de>  Mon, 09 Apr 2012 11:21:13 +0200
